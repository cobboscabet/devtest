import React, {useState} from "react";
import './App.css';
import CardDetail from './components/cardDetails/CardDetail';
import CardList from "./components/CardList/CardList";

function App() {

  const [chosenCard, setChosenCard] =useState({});

  return (
    <div className="App">
      <CardList/>
      <CardDetail card={chosenCard}/>
    </div>
  );
}

export default App;
