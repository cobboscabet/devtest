import React from "react";
import "./CardDetails.css";

/*
const CardDetail = (props) =>{
    
    return (<div className="cardDetails">
        <div className="name">{props.card.name ? props.card.name : "test name"}</div>
        <div className="typeLine">{props.card.type_line? props.card.type_line : ""}</div>
        <div className="oracle">{props.card.oracle_text ? props.card.oracle_text : "test oracle text"}</div>
        <img className="cardImg" src={props.card.image_uris ? props.card.image_uris.normal : ""} alt="card image"/>  
        <div className="flavorText">{props.card.flavor_text ? props.card.flavor_text : "Test flavor text"}</div>
        <div className="pt">{props.card.power?props.card.power : "0"}/{props.card.toughness? props.card.toughness : "0"}</div>
    </div>
    );
}
*/

class CardDetail extends React.Component{
    render (){
        return (
            <div className="cardDetails">
                <div className="name">{this.props.card.name ? this.props.card.name : "test name"}</div>
                <div className="typeLine">{this.props.card.type_line? this.props.card.type_line : ""}</div>
                <div className="oracle">{this.props.card.oracle_text ? this.props.card.oracle_text : "test oracle text"}</div>
                <img className="cardImg" src={this.props.card.image_uris ? this.props.card.image_uris.normal : ""} alt="card image"/>  
                <div className="flavorText">{this.props.card.flavor_text ? this.props.card.flavor_text : "Test flavor text"}</div>
                <div className="pt">{this.props.card.power?this.props.card.power : "0"}/{this.props.card.toughness? this.props.card.toughness : "0"}</div>
            </div>
        );
    }
}

export default CardDetail;