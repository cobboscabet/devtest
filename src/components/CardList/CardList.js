import React from "react";
import "./CardList.css";


// card list api page https://scryfall.com/docs/api/lists



/*
const CardList = ()=>{
    return (
        <div className="cardListContainer">
            <div className="cardListItem">sample item</div>
        </div> 
    ) 
}
*/

class CardList extends React.Component{

    render(){
        return (
            <div className="cardListContainer">
                <div className="cardListItem">sample item</div>
            </div> 
        ) 
    }
}


export default CardList;